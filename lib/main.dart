import 'package:flutter/material.dart';
import 'result.dart';

void main() {
  runApp(MaterialApp(
    title: "Calculadora de IMC",
    home: Homepage(),
  ));
}

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _resultText = "";

  void _resetCampos() {
    _formKey.currentState.reset();

    weightController.clear();
    heightController.clear();

    setState(() {
      _resultText = "";
    });
  }

  void _calcular() {
    setState(() {
      bool normalWeight = false;
      double weight = double.parse(weightController.text);
      double height = double.parse(heightController.text) / 100;

      double imc = weight / (height * height);

      if (imc < 18.6)
        _resultText = "Abaixo do peso (${imc.toStringAsPrecision(4)})";
      else if (imc >= 18.6 && imc < 24.9) {
        normalWeight = true;
        _resultText = "Peso ideal (${imc.toStringAsPrecision(4)})";
      } else if (imc >= 24.9 && imc < 29.9)
        _resultText = "Levemente acima do peso (${imc.toStringAsPrecision(4)})";
      else if (imc >= 29.9 && imc < 34.9)
        _resultText = "Obesidade Grau I (${imc.toStringAsPrecision(4)})";
      else if (imc >= 34.9 && imc < 39.9)
        _resultText = "Obesidade Grau II (${imc.toStringAsPrecision(4)})";
      else if (imc >= 40)
        _resultText = "Obesidade Grau III (${imc.toStringAsPrecision(4)})";

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                    imc: imc,
                    resultText: _resultText,
                    isGood: normalWeight,
                  )));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora de IMC"),
        centerTitle: true,
        backgroundColor: Colors.blue,
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _resetCampos,
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Icon(Icons.person, size: 150, color: Colors.blue),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Peso (kg)",
                      labelStyle: TextStyle(color: Colors.blue)),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blue, fontSize: 25),
                  controller: weightController,
                  validator: (value) {
                    if (value.isEmpty) return "Insira seu peso!";
                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Altura (cm)",
                      labelStyle: TextStyle(color: Colors.blue)),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blue, fontSize: 25),
                  controller: heightController,
                  validator: (value) {
                    if (value.isEmpty) return "Insira sua altura!";
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: ButtonTheme(
                    height: 50,
                    highlightColor: Colors.purple,
                    child: RaisedButton(
                      onPressed: () =>
                          {if (_formKey.currentState.validate()) _calcular()},
                      child: Text(
                        "Calcular",
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                      color: Colors.blue,
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
