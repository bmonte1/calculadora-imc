import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final imc, resultText, isGood;

  Result({this.imc, this.resultText, this.isGood});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculadora de IMC"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 100, 20, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Icon(isGood ? Icons.thumb_up : Icons.thumb_down,
                size: 150, color: isGood ? Colors.green : Colors.red),
            Text(
              "Seu IMC é ${imc.toStringAsPrecision(4)}",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 36,
                  color: isGood ? Colors.green : Colors.red,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              resultText,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                color: isGood ? Colors.green : Colors.red,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
